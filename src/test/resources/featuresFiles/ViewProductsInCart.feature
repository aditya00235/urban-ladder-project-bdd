Feature: View product present in Cart

Scenario Outline: Users should be able to view products in cart 
Given Firstly Opens the Browser
And go to urban ladder URL
When User login with "<email>" and "<password>"
And click on cart icon and click checkout
Then the product with required details appears
And lastly close the browserss

Examples:
|email						|password		|
|dummyaditya00235@gmail.com	|Aditya@1234	|