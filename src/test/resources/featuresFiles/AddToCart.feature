Feature: Add product to Cart

Scenario Outline: Users should be able to add product to cart 
Given Firstly opens the Browsers
And go to the urban ladder URL
When User login with "<email>" and "<password>" and search the product "<product>" click on product and click Add tocart
Then the product should be moved to the cart
And lastly closes the browser

Examples:
|email						|password				|product	|
|dummyaditya00235@gmail.com	|Aditya@1234			|sofa		|
