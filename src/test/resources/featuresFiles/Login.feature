Feature: Login into Application

Scenario Outline: Users should be able to login to avail different features 
Given First opens the Browsers
And go to URL
When User login into the application with "<email>" and "<password>"
Then different features should be available like adding to cart and more
And CLOSES BROWSER

Examples:
|email							|password	|
|aditya@gmail.com				|7464712345	|
|dummyaditya00235@gmail.com		|Aditya@1234|