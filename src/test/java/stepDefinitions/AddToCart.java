package stepDefinitions;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import pageobject.AddToCartPage;
import reusablecomponent.Base;
import reusablecomponent.GeneratePropertiesObject;

public class AddToCart 
{
	WebDriver driver;
	Properties prop;
	Logger log=Logger.getLogger(AskQuestions.class);
	boolean result;
	boolean result2;
	
	@Given("^Firstly opens the Browsers$")
	public void firstly_opens_the_Browsers() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver=Base.initializeDriver();
	    driver.manage().window().maximize();
	}

	@Given("^go to the urban ladder URL$")
	public void go_to_the_urban_ladder_URL() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		prop=GeneratePropertiesObject.generatePropObject();
	    driver.get(prop.getProperty("url"));
	}

	@When("^User login with \"([^\"]*)\" and \"([^\"]*)\" and search the product \"([^\"]*)\" click on product and click Add tocart$")
	public void user_login_with_and_and_search_the_product_click_on_product_and_click_Add_tocart(String arg1, String arg2, String arg3) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		result=AddToCartPage.addProductToCart(driver, log, arg1, arg2, arg3);
	}
	
	@Then("^the product should be moved to the cart$")
	public void the_product_should_be_moved_to_the_cart() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertTrue(result);
	}

	@Then("^lastly closes the browser$")
	public void lastly_closes_the_browser() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.close();
	    driver=null;
	}
}
