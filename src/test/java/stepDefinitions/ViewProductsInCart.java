package stepDefinitions;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import pageobject.CartPage;
import pageobject.LoginPage;
import reusablecomponent.Base;
import reusablecomponent.GeneratePropertiesObject;
import reusablecomponent.ReusableMethods;
import uistore.HomePageUI;

public class ViewProductsInCart 
{
	WebDriver driver;
	Properties prop;
	Logger log=Logger.getLogger(AskQuestions.class);
	boolean result;
	boolean result2;
	
	@Given("^Firstly Opens the Browser$")
	public void firstly_Opens_the_Browser() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver=Base.initializeDriver();
	    driver.manage().window().maximize();
	}

	@Given("^go to urban ladder URL$")
	public void go_to_urban_ladder_URL() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		prop=GeneratePropertiesObject.generatePropObject();
	    driver.get(prop.getProperty("url"));
	}

	
	@When("^User login with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_login_with_and(String arg1, String arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		result=LoginPage.logIn(driver, log, arg1, arg2);
	}
	

	@When("^click on cart icon and click checkout$")
	public void click_on_cart_icon_and_click_checkout() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    if(result)
	    {
	    	if(ReusableMethods.click(HomePageUI.cart, driver))
	    	{
	    		ReusableMethods.timelaps(driver);
	    		result2=CartPage.checkoutProducts(driver, log,"dummymail","password");
	    	}
	    }
	}

	@Then("^the product with required details appears$")
	public void the_product_with_required_details_appears() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertTrue(result2);
	}

	@Then("^lastly close the browserss$")
	public void lastly_close_the_browserss() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.close();
	    driver=null;
	}
}
