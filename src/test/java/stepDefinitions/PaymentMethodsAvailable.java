package stepDefinitions;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import pageobject.HomePage;
import reusablecomponent.Base;
import reusablecomponent.GeneratePropertiesObject;

public class PaymentMethodsAvailable 
{
	WebDriver driver;
	Properties prop;
	Logger log=Logger.getLogger(AskQuestions.class);
	boolean result;
	boolean result2;
	
	@Given("^User opens Browsers$")
	public void user_opens_Browsers() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver=Base.initializeDriver();
	    driver.manage().window().maximize();
	}

	@Given("^visit to the Url$")
	public void visit_to_the_Url() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		prop=GeneratePropertiesObject.generatePropObject();
	    driver.get(prop.getProperty("url"));
	}

	@When("^user go to the bottom of home page$")
	public void user_go_to_the_bottom_of_home_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    result=HomePage.weAcceptPayments(driver, log);
	}

	@Then("^user should be able to view different payments methods available$")
	public void user_should_be_able_to_view_different_payments_methods_available() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertEquals(true, result);
	}

	@Then("^exit browsers$")
	public void exit_browsers() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.close();
	    driver=null;
	}
}
