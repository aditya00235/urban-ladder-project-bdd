package stepDefinitions;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import pageobject.HomePage;
import pageobject.UserStoriesPage;
import reusablecomponent.Base;
import reusablecomponent.GeneratePropertiesObject;

public class UserStories 
{
	WebDriver driver;
	Properties prop;
	Logger log=Logger.getLogger(AskQuestions.class);
	boolean result;
	boolean result2;
	

	@Given("^User open Browsers$")
	public void user_open_Browsers() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver=Base.initializeDriver();
	    driver.manage().window().maximize();
	}

	@Given("^visit to the url$")
	public void visit_to_the_url() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		prop=GeneratePropertiesObject.generatePropObject();
	    driver.get(prop.getProperty("url"));
	}

	@When("^user hit the view all user stories button$")
	public void user_hit_the_view_all_user_stories_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    result=HomePage.checkUserStories(driver, log);
	}

	@Then("^user should be able to view all users stories$")
	public void user_should_be_able_to_view_all_users_stories() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(result);
	}

	@Then("^exit browser$")
	public void exit_browser() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.close();
	    driver=null;
	}
}
