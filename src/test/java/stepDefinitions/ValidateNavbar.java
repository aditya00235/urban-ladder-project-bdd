package stepDefinitions;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import pageobject.HomePage;
import reusablecomponent.Base;
import reusablecomponent.GeneratePropertiesObject;

public class ValidateNavbar 
{
	WebDriver driver;
	Properties prop;
	Logger log=Logger.getLogger(AskQuestions.class);
	boolean result;
	boolean result2;
	
	@Given("^First opens Browsers$")
	public void first_opens_Browsers() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver=Base.initializeDriver();
	    driver.manage().window().maximize();
	}

	@Given("^go to url$")
	public void go_to_url() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		prop=GeneratePropertiesObject.generatePropObject();
	    driver.get(prop.getProperty("url"));
	}

	@When("^user naviagate to home page$")
	public void user_naviagate_to_home_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("^user should be able to view the navbar and use different features available there$")
	public void user_should_be_able_to_view_the_navbar_and_use_different_features_available_there() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    result=HomePage.validateNavbar(driver, log);
	    Assert.assertEquals(true, result);
	}

	@Then("^CLOSE BROWSER$")
	public void close_BROWSER() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.close();
	    driver=null;
	}
}
