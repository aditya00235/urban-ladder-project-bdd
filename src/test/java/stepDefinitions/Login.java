package stepDefinitions;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import pageobject.LoginPage;
import reusablecomponent.Base;
import reusablecomponent.GeneratePropertiesObject;

public class Login
{
	WebDriver driver;
	Properties prop;
	Logger log=Logger.getLogger(AskQuestions.class);
	boolean result;
	boolean result2;
	
	@Given("^First opens the Browsers$")
	public void first_opens_the_Browsers() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver=Base.initializeDriver();
	    driver.manage().window().maximize();
	}

	@Given("^go to URL$")
	public void go_to_URL() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		prop=GeneratePropertiesObject.generatePropObject();
	    driver.get(prop.getProperty("url"));
	}

	@When("^User login into the application with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_login_into_the_application_with_and(String arg1, String arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    if(LoginPage.logIn(driver, log, arg1, arg2))
	    {
	    	result=true;
	    }
	    else {
	    	result=false;
	    }
	}

	@Then("^different features should be available like adding to cart and more$")
	public void different_features_should_be_available_like_adding_to_cart_and_more() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		if(result)
		{
	    Assert.assertEquals(true, result);
		}
		else {
			Assert.assertEquals(false, result);
		}
	}

	@Then("^CLOSES BROWSER$")
	public void closes_BROWSER() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.close();
	    driver=null;
	}
}
